import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Bill} from '../models/bill.model';
import {BaseUrl} from '../core/base-url';

@Injectable()
export class BillService extends BaseUrl {

  resultUrl: string;

  constructor(private http: HttpClient) {
    super();
  }

  getBill(): Observable<Bill> {
    this.resultUrl = this.baseUrl + '/bill';
    return this.http.get<Bill>(this.resultUrl);
  }

  updateBill(bill: Bill): Observable<Bill> {
    this.resultUrl = this.baseUrl + '/bill';
    return this.http.put<Bill>(this.resultUrl, bill);
  }

  getCurrency(base: string = 'RUB'): Observable<any> {
    return this.http.get(`https://exchangeratesapi.io/api/latest?base=${base}`);
  }

}
