import {BaseUrl} from '../core/base-url';
import {HttpClient} from '../../../../../node_modules/@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from '../models/category.model';
import {Injectable, OnInit} from '@angular/core';

@Injectable()
export class CategoryService extends BaseUrl implements OnInit {

  resultUrl: string;

  constructor(private http: HttpClient) {
    super();
  }

  ngOnInit() {
  }

  addCategory(category: Category): Observable<Category> {
    this.resultUrl = this.baseUrl + `/categories`;
    return this.http.post<Category>(this.resultUrl, category);
  }

  getCategories(): Observable<Category[]> {
    this.resultUrl = this.baseUrl + `/categories`;
    return this.http.get<Category[]>(this.resultUrl);
  }

  updateCategory(category: Category): Observable<Category> {
    this.resultUrl = this.baseUrl + `/categories/${category.id}`;
    return this.http.put<Category>(this.resultUrl, category);
  }

  getCategoryById(id: number): Observable<Category> {
    this.resultUrl = this.baseUrl + `/categories/${id}`;
    return this.http.get<Category>(this.resultUrl);
  }

}
