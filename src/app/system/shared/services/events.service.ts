import {Injectable} from '@angular/core';
import {BaseUrl} from '../../../shared/core/base-url';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {WFMEvent} from '../models/event.model';

@Injectable()
export class EventsService extends BaseUrl {

  resultUrl: string;

  constructor(private http: HttpClient) {
    super();
  }

  addEvent(event: WFMEvent): Observable<WFMEvent> {
    this.resultUrl = this.baseUrl + `/events`;
    return this.http.post<WFMEvent>(this.resultUrl, event);
  }

  getEvents(): Observable<WFMEvent[]> {
    this.resultUrl = this.baseUrl + `/events`;
    return this.http.get<WFMEvent[]>(this.resultUrl);
  }

  getEventById(id: string): Observable<WFMEvent> {
    this.resultUrl = this.baseUrl + `/events/${id}`;
    return this.http.get<WFMEvent>(this.resultUrl);
  }
}
