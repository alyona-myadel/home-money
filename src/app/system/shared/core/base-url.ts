import {Injectable} from '@angular/core';

@Injectable()
export class BaseUrl {

  baseUrl: string;

  constructor() {
    this.baseUrl = 'http://localhost:3000';

  }
}
