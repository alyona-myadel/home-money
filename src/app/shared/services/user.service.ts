import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../models/user.model';
import {map} from 'rxjs/operators';
import {BaseUrl} from '../core/base-url';

@Injectable()
export class UserService extends BaseUrl {

  resultUrl: string;

  constructor(private http: HttpClient) {
    super();
  }

  getUserByEmail(email: string): Observable<User> {
    this.resultUrl = this.baseUrl + `/users?email=${email}`;
    return this.http.get<User>(this.resultUrl).pipe(map((user: User[]) => user[0] ? user[0] : undefined));
  }

  createNewUser(user: User): Observable<User> {
    this.resultUrl = this.baseUrl + `/users`;
    return this.http.post<User>(this.resultUrl, user);
  }
}
